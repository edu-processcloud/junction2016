<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>

    </ul>
</nav>
<div class="arrivals index large-9 medium-8 columns content">
    <h3><?= __('Arrivals') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('identificator') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($arrivals as $arrival): ?>
            <tr>
                <td><?= h($arrival->id) ?></td>
                <td><?= h($arrival->identificator) ?></td>
                <td><?= h($arrival->created) ?></td>
                <td><?= h($arrival->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $arrival->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $arrival->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $arrival->id], ['confirm' => __('Are you sure you want to delete # {0}?', $arrival->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
