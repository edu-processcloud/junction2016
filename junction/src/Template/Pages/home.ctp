<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;

if (!Configure::read('debug')):
    throw new NotFoundException('Please replace src/Template/Pages/home.ctp with your own version.');
endif;

$cakeDescription = 'lastLEG';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>
</head>
<body class="home">
    <header>
        <div class="header-image">
            <?= $this->Html->image('https://challengepost-s3-challengepost.netdna-ssl.com/photos/production/software_photos/000/450/705/datas/gallery.jpg') ?>
            <h1>lastLEG</h1>
        </div>
    </header>
    <div id="content">

        <div class="row">
            <div class="columns large-6">
                <h3>this site was build to demonstrate lastLEG data collection</h3>
                <ul>
                    <li>check DEvpost, bitbucket and junction 2016 for more information</li>
                </ul>
            </div>
            <div class="columns large-6">
                <h3>Check the project</h3>
                <ul>
                    <li><a target="_blank" href="https://upm.videosync.fi/2016-11-10-junction-preevent?seek=1827">UPM challenge</a></li>
                    <li><a href="https://junction.processcloud.xyz/arrivals">Arrivals data</a></li>
                    <li><a target="_blank" href="https://devpost.com/software/lastleg/">lastLEG | Devpost</a></li>
                    <li><a target="_blank" href="https://bitbucket.org/edu-processcloud/junction2016">bitbucket</a></li>
                    <li><a target="_blank" href="http://live.hackjunction.com/">junction 2016</a></li>
                </ul>
                <p>
            </div>
        </div>
        <hr/>
    </div>
</body>
</html>
